package koval.vtc.ua.vtctests.datamodels;

/**
 * This is key construction
 */

public class KeyDataModel {
    private Integer id;
    private String VALUE_KEY;
    private Integer STATUS_KEY;

    KeyDataModel(){}

    public KeyDataModel(Integer id, String VALUE_KEY, Integer STATUS_KEY){
        this.id = id;
        this.VALUE_KEY = VALUE_KEY;
        this.STATUS_KEY = STATUS_KEY;
    }
    public Integer getId(){return id;}
    public void setId(Integer id){this.id = id;}
    public String getValueKey(){return VALUE_KEY;}
    public void setValueKey(String VALUE_KEY){this.VALUE_KEY = VALUE_KEY;}
    public Integer getStatusKey(){return STATUS_KEY;}
    public void setStatusKey(Integer STATUS_KEY){this.STATUS_KEY = STATUS_KEY;}
}
