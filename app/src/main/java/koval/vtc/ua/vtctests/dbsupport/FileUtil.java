package koval.vtc.ua.vtctests.dbsupport;

import android.support.annotation.NonNull;

import com.google.firebase.crash.FirebaseCrash;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created by Vitalii on 08.12.2017.
 */

public class FileUtil {
    public static boolean downloadFile(@NonNull final String url, @NonNull final String pathToFile) {

        try {
            URL urls = new URL(url);
            BufferedInputStream bis = new BufferedInputStream(urls.openStream());

            FileOutputStream fis = new FileOutputStream(pathToFile);
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = bis.read(buffer, 0, 1024)) != -1) {
                fis.write(buffer, 0, count);
            }
            //TODO bad experience, see autocloseable and try with resources


            try {
                fis.close();
            } catch (IOException e) {
                FirebaseCrash.report(e);
            }


            try {
                bis.close();
            } catch (IOException e) {
                FirebaseCrash.report(e);
            }


            return true;
        } catch (IOException e) {
            FirebaseCrash.report(e);
            return false;
        }

    }

    public static void eraise(String... files) {
        for (int i = 0; i < files.length; i++) {
            File file = new File(files[i]);
            try{
                file.delete();
            }catch (RuntimeException e){
                FirebaseCrash.report(e);
            }

        }
    }
}
