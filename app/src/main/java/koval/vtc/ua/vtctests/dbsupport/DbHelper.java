package koval.vtc.ua.vtctests.dbsupport;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigInfo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


/**
 * This is DataBaseHelper class for creating and updating database, also for get access to DB.
 */

public class DbHelper extends SQLiteOpenHelper{

    private static String DB_PATH = "/data/data/koval.vtc.ua.vtctests/databases/";
    private static String DB_DPATH = "/data/data/koval.vtc.ua.vtctests/files";
    private static String DB_NAME = "TestsDB.sqlite";
    private SQLiteDatabase myDataBase;
    private final Context mContext;
    public static String TABLE_QUESTIONS = "tests_db";
    public static String TABLE_ANSWERS = "answers_db";
    public static String TABLE_KEYS = "keys_db";

    public static String COLUMN_MAIN_ID = "_id";

    public static String COLUMN_QUESTIONS_QUESTION_VALUE = "questionValue";
    public static String COLUMN_QUESTIONS_ANSWER_ID = "answerID";
    public static String COLUMN_QUESTIONS_ANSWERS_GROUP_ID = "answersGroupID";

    public static String COLUMN_ANSWERS_ANSWER_VALUE = "answerValue";
    public static String COLUMN_ANSWERS_ANSWER_ID_MAIN = "answerIDmain";
    public static String COLUMN_ANSWERS_ANSWER_GROUP_ID_MAIN = "answerGroupIDmain";

    public static String COLUMN_KEYS_KEY_VALUE = "value";
    public static String COLUMN_KEYS_KEY_STATUS = "status";
    public DbHelper(Context context){ // Constructor
        super(context, DB_NAME,null,1);
        this.mContext = context;
    }

    public void createDataBase() throws IOException {
            this.getReadableDatabase();
            try {
               copyDataBase();
            } catch (IOException e) {
                FirebaseCrash.report(e);
            }

        }


    private void copyDataBase() throws IOException{ //Copy DB f
        FileInputStream myInput = new FileInputStream(DB_DPATH + "/" + DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int lenght;
        while ((lenght = myInput.read(buffer))>0){
            myOutput.write(buffer, 0 ,lenght);
        }
        try{
            myOutput.flush();
        }catch (IOException e){
            FirebaseCrash.report(e);
        }
        try{
            myOutput.close();
        }catch (IOException e){
            FirebaseCrash.report(e);
        }
        try{
            myInput.close();
        }catch (IOException e){
            FirebaseCrash.report(e);
        }
    }

    public void openDataBase() throws SQLException{
        String mypath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(mypath, null,SQLiteDatabase.OPEN_READONLY);
    }
    @Override
    public synchronized void close(){
        if(myDataBase != null){
            myDataBase.close();
        }
        super.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db){}
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){}
}

