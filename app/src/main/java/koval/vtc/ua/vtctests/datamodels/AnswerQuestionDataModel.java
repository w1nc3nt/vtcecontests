package koval.vtc.ua.vtctests.datamodels;

/**
 * Created by Evil on 07.12.2017.
 */

public class AnswerQuestionDataModel {
    private Integer id;
    private String ANSWER_VALUE;
    private String QUESTION_VALUE;
    private Boolean ANSWER_RESULT;

    AnswerQuestionDataModel(){}

    public AnswerQuestionDataModel(Integer id, String ANSWER_VALUE, String QUESTION_VALUE, Boolean ANSWER_RESULT){
        this.id = id;
        this.ANSWER_VALUE = ANSWER_VALUE;
        this.QUESTION_VALUE = QUESTION_VALUE;
        this.ANSWER_RESULT = ANSWER_RESULT;
    }

    public Integer getId(){return id;}
    public void setId(Integer id){this.id = id;}
    public String getAnswerValue(){return ANSWER_VALUE;}
    public void setAnswerValue(String ANSWER_VALUE){this.ANSWER_VALUE = ANSWER_VALUE;}
    public String getQuestionValue(){return QUESTION_VALUE;}
    public void setAnswerResult(Boolean ANSWER_RESULT){this.ANSWER_RESULT = ANSWER_RESULT;}
    public Boolean getAnswerResult(){return ANSWER_RESULT;}
}
