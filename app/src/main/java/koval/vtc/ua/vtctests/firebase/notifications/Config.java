package koval.vtc.ua.vtctests.firebase.notifications;

/**
 * Created by Evil on 09.01.2018.
 */

public class Config {
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String NOTIFICATION_CHANNEL = "timer_notification_channel";
    public static final String NOTIFICATION_CHANNEL_DESCRIPTION = "Timer channel";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
}
