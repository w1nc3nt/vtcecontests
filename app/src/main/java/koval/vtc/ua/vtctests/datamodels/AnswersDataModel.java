package koval.vtc.ua.vtctests.datamodels;

/**
 * This is answer construction.
 */

public class AnswersDataModel {
    private Integer id;
    private String ANSWER_VALUE;
    private Integer ANSWER_ID_MAIN;
    private Integer ANSWER_GROUP_ID_MAIN;

    AnswersDataModel(){}

    public AnswersDataModel(Integer id, String ANSWER_VALUE, Integer ANSWER_ID_MAIN, Integer ANSWER_GROUP_ID_MAIN){
        this.id = id;
        this.ANSWER_VALUE = ANSWER_VALUE;
        this.ANSWER_ID_MAIN = ANSWER_ID_MAIN;
        this.ANSWER_GROUP_ID_MAIN = ANSWER_GROUP_ID_MAIN;
    }
    public Integer getId(){return id;}
    public void setId(Integer id){this.id = id;}
    public String getAnswerValue(){return ANSWER_VALUE;}
    public void setAnswerValue(String ANSWER_VALUE){this.ANSWER_VALUE = ANSWER_VALUE;}
    public Integer getAnswerIdMain(){return ANSWER_ID_MAIN;}
    public void setAnswerIdMain(Integer ANSWER_ID_MAIN) {this.ANSWER_ID_MAIN = ANSWER_ID_MAIN;}
    public Integer getAnswerGroupIdMain(){return ANSWER_GROUP_ID_MAIN;}
    public void setAnswerGroupIdMain(Integer ANSWER_GROUP_ID_MAIN){this.ANSWER_GROUP_ID_MAIN = ANSWER_GROUP_ID_MAIN;}
}
