package koval.vtc.ua.vtctests.datamodels;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.view.LayoutInflater;
import java.util.List;
import koval.vtc.ua.vtctests.R;

/**
 * Created by egork on 14.12.2017.
 */

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.QuestionsAnswersViewHolder> {

    public static class QuestionsAnswersViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView question_value;
        Button answe_value;

        QuestionsAnswersViewHolder(View item){
            super(item);
            cv = (CardView) item.findViewById(R.id.cardVi);
            question_value = (TextView) item.findViewById(R.id.textCardQues);
            answe_value = (Button) item.findViewById(R.id.buttonCardAnsw);
        }
    }

    List<AnswerQuestionDataModel> questions_answers;
    public RVAdapter(List<AnswerQuestionDataModel> questions_answers){
        this.questions_answers = questions_answers;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public QuestionsAnswersViewHolder onCreateViewHolder(ViewGroup viewGroup,int i){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.post_cardview, viewGroup, false);
        QuestionsAnswersViewHolder qavh = new QuestionsAnswersViewHolder(v);
        return qavh;
    }

    @Override
    public void onBindViewHolder(QuestionsAnswersViewHolder questionsAnswersViewHolder, int i) {
        questionsAnswersViewHolder.question_value.setText(questions_answers.get(i).getQuestionValue());
        questionsAnswersViewHolder.answe_value.setText(questions_answers.get(i).getAnswerValue());
        if (questions_answers.get(i).getAnswerResult().equals(true)) {
            questionsAnswersViewHolder.answe_value.setBackgroundResource(R.drawable.shape_correct);
        } else {
            questionsAnswersViewHolder.answe_value.setBackgroundResource(R.drawable.shape_wrong);
        }
    }

    @Override
    public int getItemCount(){
        return questions_answers.size();
    }

}
