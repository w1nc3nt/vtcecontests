package koval.vtc.ua.vtctests.datamodels;

/**
 * This is questions construction.
 */

public class QuestionsDataModel {
    private Integer id;
    private String QUESTION_VALUE;
    private Integer ANSWER_ID;
    private Integer ANSWERS_GROUP_ID;

    QuestionsDataModel(){}

    public QuestionsDataModel(Integer id, String QUESTION_VALUE, Integer ANSWER_ID, Integer ANSWERS_GROUP_ID){
        this.id = id;
        this.QUESTION_VALUE = QUESTION_VALUE;
        this.ANSWER_ID = ANSWER_ID;
        this.ANSWERS_GROUP_ID = ANSWERS_GROUP_ID;
    }
    public Integer getId() {return id;}
    public void setId(Integer id) {this.id = id;}
    public String getQuestionValue() {return QUESTION_VALUE;}
    public void setQuestionValue(String QUESTION_VALUE) {this.QUESTION_VALUE = QUESTION_VALUE;}
    public Integer getAnswerId() {return ANSWER_ID;}
    public void setAnswerId(Integer ANSWER_ID) {this.ANSWER_ID = ANSWER_ID;}
    public Integer getAnswersGroupId() {return ANSWERS_GROUP_ID;}
    public void setAnswersGroupId(Integer ANSWERS_GROUP_ID){this.ANSWERS_GROUP_ID = ANSWERS_GROUP_ID;}
}
