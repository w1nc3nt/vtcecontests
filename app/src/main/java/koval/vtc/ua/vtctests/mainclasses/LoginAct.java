package koval.vtc.ua.vtctests.mainclasses;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.perf.metrics.AddTrace;

import koval.vtc.ua.vtctests.thread.AsyncDownload;
import koval.vtc.ua.vtctests.CustomToast;
import koval.vtc.ua.vtctests.R;


public class LoginAct extends Fragment implements View.OnClickListener {

    public ProgressDialog waitDialogAuth;
    public static final String firebaseReportPrefix = "[LoginAct]";
    public View view;
    public EditText password;
    private Button loginButton;
    private CheckBox show_hide_password;
    private LinearLayout loginLayout;
    private Animation shakeAnimation;
    public FragmentManager fragmentManager;
    private Boolean isClicked = false;
    private AsyncTask asyncTask;


    @Override
    @AddTrace(name = firebaseReportPrefix+"onCreateView")
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_layout, container, false);
        initViews();
        setListeners();
        if (savedInstanceState != null) {
            Log.d("TAG","savedInstanceState !=null");
            isClicked = savedInstanceState.getBoolean(getString(R.string.saved_isClicked));
            password.setText(savedInstanceState.getString(getString(R.string.saved_password)));

        }
        return view;
    }

    @Override
    @AddTrace(name = firebaseReportPrefix+"onCreate")
    public void onCreate(Bundle savedInstatnce) {
        super.onCreate(savedInstatnce);
    }

    @AddTrace(name = firebaseReportPrefix+"onSaveInstanceState")
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("TAG","onSaveInstanceState");
        outState.putBoolean(getString(R.string.saved_isClicked), isClicked);
        outState.putString(getString(R.string.saved_password), password.getText().toString());

    }

    @AddTrace(name = firebaseReportPrefix+"initViews")
    private void initViews() {
        fragmentManager = getActivity().getSupportFragmentManager();

        password = (EditText) view.findViewById(R.id.login_password);
        loginButton = (Button) view.findViewById(R.id.loginBtn);
        show_hide_password = (CheckBox) view.findViewById(R.id.show_hide_password);
        loginLayout = (LinearLayout) view.findViewById(R.id.login_layout);

        // Load ShakeAnimation
        shakeAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);

        try {
            // Setting text selector over textviews
            @SuppressLint("ResourceType") XmlResourceParser xrp = getResources().getXml(R.drawable.text_selector);
            ColorStateList csl = ColorStateList.createFromXml(getResources(), xrp);
            show_hide_password.setTextColor(csl);
        } catch (Exception e) {
            FirebaseCrash.report(e);
        }
    }

    /**
     * Проверяем есть ли подключение к интернету. Возвращает true если есть, false - если нету.
     *
     * @param context
     * @return
     */

    @AddTrace(name = firebaseReportPrefix+"isOnline")
    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    @AddTrace(name = firebaseReportPrefix+"setListeners")
    private void setListeners() {
        loginButton.setOnClickListener(this);

        // Set check listener over checkbox for showing and hiding password
        show_hide_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean isChecked) {
                // If it is checked then show password else hide
                // password
                if (isChecked) {
                    show_hide_password.setText(R.string.hide_pwd);// change
                    // checkbox
                    // text
                    password.setInputType(InputType.TYPE_CLASS_TEXT);
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());// show password
                } else {
                    show_hide_password.setText(R.string.show_pwd);// change
                    // checkbox
                    // text
                    password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());// hide password

                }

            }
        });
    }

    @AddTrace(name = firebaseReportPrefix+"onClick")
    public void onClick(View vi) {
        switch (vi.getId()) {
            case R.id.loginBtn:
                doLogin();
        }
    }

    @AddTrace(name = firebaseReportPrefix+"doLogin")
    private void doLogin() {
        if (isOnline(getContext())) {
            isClicked = true;
            String databasePath = getContext().getFilesDir().getPath() + getString(R.string.db_file_name);
            asyncTask = new AsyncDownload(this).execute(getString(R.string.db_url), databasePath);
            Log.d("TAG", "Async started form doLogin()");
        } else {
            new CustomToast().Show_Toast(getActivity(), view, getString(R.string.toast_server_not_response));
        }
    }


    @Override
    @AddTrace(name = firebaseReportPrefix+"onResume")
    public void onResume() {
        super.onResume();
        Log.d("TAG","onResume()");
        if (isClicked) {
            asyncTask = new AsyncDownload(this).execute(getString(R.string.db_url), getContext().getFilesDir().getPath() + getString(R.string.db_file_name));
            isClicked = false;
        }
    }

    @Override
    @AddTrace(name = firebaseReportPrefix+"onPause")
    public void onPause(){
        super.onPause();
        Log.d("TAG","onPause()");
        if(isClicked) {
            waitDialogAuth.dismiss();
            asyncTask.cancel(true);
        }
    }

}