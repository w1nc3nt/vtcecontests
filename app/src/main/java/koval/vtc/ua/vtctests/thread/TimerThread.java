package koval.vtc.ua.vtctests.thread;


import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import koval.vtc.ua.vtctests.mainclasses.TestsingsActivity;


public class TimerThread implements Runnable {
    Thread thread;
    volatile boolean activityState;
    volatile long timeMillis = 1000L;
    volatile boolean runStatus = true;
    volatile WeakReference<TestsingsActivity> activityWeakReference;

    public TimerThread(boolean activityState, TestsingsActivity testsingsActivity){
        this.activityState = activityState;
        thread = new Thread(this);
        activityWeakReference = new WeakReference<TestsingsActivity>(testsingsActivity);
    }

    public void setTimeMillis(Long timeMillis){this.timeMillis = timeMillis;}

    public Thread getThread(){return thread;}

    public void setNewActivity(TestsingsActivity testsingsActivity){
        activityWeakReference = new WeakReference<TestsingsActivity>(testsingsActivity);
    }

    public void setRunStatus(boolean runStatus){this.runStatus = runStatus;} //Must be 1000L>

    public void setActivityState(boolean activityState){this.activityState = activityState;} //TODO: Fix that method

    @Override
    public void run() {
        int isDone = 0;
        while(runStatus) {
            TestsingsActivity testsingsActivity = activityWeakReference.get();
            timeMillis = timeMillis - 1000L;
            if(timeMillis <=300000L){isDone = 1;}
            if(timeMillis == 1000L){isDone = 2;}
            try{
                Thread.sleep(1000);
            }catch (InterruptedException e){FirebaseCrash.report(e);}
            setTimerTime(timeMillis, testsingsActivity, isDone);
        }
    }


    private void setTimerTime(Long timeInMilliseconds, TestsingsActivity testsingsActivity, int isDone){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMilliseconds);
        if(activityState) {
            sendMessage(simpleDateFormat.format(calendar.getTime()), testsingsActivity, isDone);
        }
    }

    private void sendMessage(String time, TestsingsActivity testsingsActivity, int isDone){
        Bundle bundle = new Bundle();
        bundle.putString("currTime", time);
        bundle.putInt("isDone",isDone);
        Message message = new Message();
        message.setData(bundle);
        testsingsActivity.handler.sendMessage(message);
    }
}
