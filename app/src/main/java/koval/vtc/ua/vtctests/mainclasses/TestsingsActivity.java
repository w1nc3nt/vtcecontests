package koval.vtc.ua.vtctests.mainclasses;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.perf.metrics.AddTrace;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import koval.vtc.ua.vtctests.thread.AsyncSender;
import koval.vtc.ua.vtctests.DialogView;
import koval.vtc.ua.vtctests.R;
import koval.vtc.ua.vtctests.datamodels.AnswerQuestionDataModel;
import koval.vtc.ua.vtctests.datamodels.AnswersDataModel;
import koval.vtc.ua.vtctests.datamodels.QuestionsDataModel;
import koval.vtc.ua.vtctests.datamodels.RVAdapter;
import koval.vtc.ua.vtctests.dbsupport.DbActions;
import koval.vtc.ua.vtctests.dbsupport.DbHelper;
import koval.vtc.ua.vtctests.thread.TimerThread;

public class TestsingsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    TextView name;
    TextView group;
    TextView timer;
    LayoutInflater layInflat;
    LinearLayout linLayout;
    View.OnClickListener listener;
    QuestionsDataModel question;
    AnswersDataModel answer1;
    AnswersDataModel answer2;
    AnswersDataModel answer3;
    AnswersDataModel answer4;
    public List<AnswerQuestionDataModel> answerQuestionDataList = new ArrayList<AnswerQuestionDataModel>();
    DbActions db;
    public String resultforSending = "";
    View tmpview;
    public String names;
    String vrementext;
    public static final String firebaseReportPrefix = "[TestsingsActivity]";
    int vremenid;
    public int countOfTrueAnswers = 0;
    int isStart = 0;
    int countOfQuestions = 1;
    public boolean isSent = true;
    public boolean ifSent = false;
    public int grade = 0;
    int[] arrOfQues;
    boolean isClicked = false;
    public AsyncTask asyncSender;
    public ProgressDialog waitingDialog;
    TimerThread timerThread;
    public Handler handler;
    public String currentTime ="30:00";
    boolean lessThenFive = false;

    @Override
    @AddTrace(name = firebaseReportPrefix+"onCreate")
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testsings);
        db = new DbActions(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab); fab.hide();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextQuestion(vrementext,vremenid);
                fab.hide();
            }
        });
        listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tmpview != null){tmpview.setBackgroundResource(R.drawable.shape_item);}
                tmpview = view;
                view.setBackgroundResource(R.drawable.shape_item_selected);
                if(view instanceof Button){
                    vrementext = ((Button) view).getText().toString();
                    vremenid = view.getId();
                }
                fab.show();
            }
        };

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        initializeAnswers();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Intent in = getIntent();
        initializeViews(navigationView,in);
        startTimer(1800000L); //Start timer on 30 mins
        handler = initializeHandler();
        if(timer !=null){timer.setText("SUKA BLYAT NE ROBIT SUK");}
    }

    private TestsingsActivity getActivity(){
        return this;
    }

    private void startTimer(Long millis){
        timerThread = new TimerThread(true,this);
        timerThread.setTimeMillis(millis);
    }

    @SuppressLint("HandlerLeak")
    private Handler initializeHandler(){
        return new Handler(){
            public void handleMessage(Message message){
                if(message.getData().getInt("isDone") == 2){
                    timerThread.setRunStatus(false);
                    countOfQuestions = 30; //Sets it for start AsyncSender
                    nextQuestion(vrementext,vremenid); //Starts AsyncSender
                }else{
                    currentTime = message.getData().getString("currTime");
                    if(timer !=null){timer.setText(currentTime);} //TODO: Not works! Need to investigate
                }
                if(message.getData().getInt("isDone") == 1 && (timer !=null)){
                    getActivity().timer.setTextColor(Color.RED); //TODO: Not works! Need to investigate
                    lessThenFive = true;
                }
            }
        };
    }

    private void initializeViews(NavigationView navigationView, Intent in){
        name = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nameVi);
        group = (TextView) navigationView.getHeaderView(0).findViewById(R.id.groupVi);
        name.setText(in.getStringExtra(getString(R.string.intent_username)) + " " + in.getStringExtra(getString(R.string.intent_userlastname)));
        group.setText(in.getStringExtra(getString(R.string.intent_usergroup)));
        names = name.getText().toString() + " | Группа: " + group.getText().toString();
        arrOfQues = getRandQuestion();
    }


    private void initializeAnswers(){
        question = new QuestionsDataModel(null,null,null,null);
        answer1 = new AnswersDataModel(null,null,null,null);
        answer2 = new AnswersDataModel(null,null,null,null);
        answer3 = new AnswersDataModel(null,null,null,null);
        answer4 = new AnswersDataModel(null,null,null,null);
    }

    @AddTrace(name = firebaseReportPrefix+"onSaveInstanceState")
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (asyncSender != null) {
            asyncSender.cancel(false);
            waitingDialog.dismiss();
        }
        putData(outState);
        ArrayList<String> answer = new ArrayList<String>();
        ArrayList<String> questions = new ArrayList<String>();
        boolean[] results = new boolean[answerQuestionDataList.size()];
        for(int i = 0; i < answerQuestionDataList.size();i++){
            answer.add(answerQuestionDataList.get(i).getAnswerValue());
            questions.add(answerQuestionDataList.get(i).getQuestionValue());
            results[i] = answerQuestionDataList.get(i).getAnswerResult();
        }
        outState.putStringArrayList(getString(R.string.saved_ques_array),questions);
        outState.putStringArrayList(getString(R.string.saved_answer_array),answer);
        outState.putBooleanArray(getString(R.string.saved_answer_result),results);
    }

    private void putData(Bundle outState){
        outState.putIntArray(getString(R.string.saved_arrOfQues),arrOfQues);
        outState.putInt(getString(R.string.saved_isStart), isStart);
        outState.putBoolean(getString(R.string.saved_isClicked), isClicked);
        outState.putInt(getString(R.string.saved_countOfQues), countOfQuestions);
        outState.putInt(getString(R.string.saved_countOfTrueQues),countOfTrueAnswers);
        outState.putString(getString(R.string.saved_resultForSent), resultforSending);
        outState.putBoolean(getString(R.string.saved_isSent), isSent);
        outState.putBoolean(getString(R.string.saved_ifSent), ifSent);
        outState.putString(getString(R.string.saved_currentTime),currentTime);
        outState.putInt(getString(R.string.saved_grade), grade);
        outState.putBoolean(getString(R.string.saved_lessThenFive),lessThenFive);
    }

    @AddTrace(name = firebaseReportPrefix+"onRestoreInstanceState")
    protected void onRestoreInstanceState(Bundle restoreState){
        super.onRestoreInstanceState(restoreState);
        restoreData(restoreState);
        if(timerThread != null){
            timerThread.setActivityState(true);
            timerThread.setNewActivity(getActivity());
        }
        for(int i = 0;i < restoreState.getStringArrayList(getString(R.string.saved_ques_array)).size();i++){
            answerQuestionDataList.add(new AnswerQuestionDataModel(i,restoreState.getStringArrayList(getString(R.string.saved_answer_array)).get(i),restoreState.getStringArrayList(getString(R.string.saved_ques_array)).get(i),
                    restoreState.getBooleanArray(getString(R.string.saved_answer_result))[i]));
        }
        if(isClicked) {
            asyncSender = new AsyncSender(this).execute(); isClicked =false;}
        else if(countOfQuestions <= 30 && isStart != 0){
            addQuestion(arrOfQues[countOfQuestions]);
        }else if(grade != 0){
            grade = restoreState.getInt(getString(R.string.saved_grade));
            addResult(answerQuestionDataList);
        }
    }

    private void restoreData(Bundle restoreState){
        countOfQuestions = restoreState.getInt(getString(R.string.saved_countOfQues));
        arrOfQues = restoreState.getIntArray(getString(R.string.saved_arrOfQues));
        isStart = restoreState.getInt(getString(R.string.saved_isStart));
        countOfTrueAnswers = restoreState.getInt(getString(R.string.saved_countOfTrueQues));
        resultforSending = restoreState.getString(getString(R.string.saved_resultForSent));
        isSent = restoreState.getBoolean(getString(R.string.saved_isSent));
        ifSent = restoreState.getBoolean(getString(R.string.saved_ifSent));
        isClicked = restoreState.getBoolean(getString(R.string.saved_isClicked));
        currentTime = restoreState.getString(getString(R.string.saved_currentTime));
        lessThenFive = restoreState.getBoolean(getString(R.string.saved_lessThenFive));
    }


    @AddTrace(name = firebaseReportPrefix+"addQuestion")
    public void addQuestion(int id){ //Inflating new Question and 3 or 4 answers.
        getDbQuestAnswer(id);
        linLayout = (LinearLayout) findViewById(R.id.quesLay);
        layInflat = getLayoutInflater();
        View item = layInflat.inflate(R.layout.model_ques_item, linLayout,false);
        timer = (TextView) item.findViewById(R.id.textTimer); timer.setText(currentTime); if(lessThenFive){timer.setTextColor(Color.RED);} //TODO: Works! Must be not!
        TextView countOfQuest = (TextView) item.findViewById(R.id.countOfQues); countOfQuest.setText(Integer.toString(countOfQuestions) + " з 30");
        TextView itemQuestion = (TextView) item.findViewById(R.id.etQuesti); itemQuestion.setText(question.getQuestionValue()); itemQuestion.setId(question.getAnswerId());
        Button bAnswer1 = (Button) item.findViewById(R.id.bAnswer1); bAnswer1.setText(answer1.getAnswerValue()); bAnswer1.setOnClickListener(listener);
        bAnswer1.setId(answer1.getAnswerIdMain());
        Button bAnswer2 = (Button) item.findViewById(R.id.bAnswer2); bAnswer2.setText(answer2.getAnswerValue()); bAnswer2.setOnClickListener(listener);
        bAnswer2.setId(answer2.getAnswerIdMain());
        Button bAnswer3 = (Button) item.findViewById(R.id.bAnswer3); bAnswer3.setText(answer3.getAnswerValue()); bAnswer3.setOnClickListener(listener);
        bAnswer3.setId(answer3.getAnswerIdMain());
        Button bAnswer4 = (Button) item.findViewById(R.id.bAnswer4);
        if(answer4.getAnswerValue() != null){
         bAnswer4.setText(answer4.getAnswerValue()); bAnswer4.setOnClickListener(listener); bAnswer4.setId(answer4.getAnswerIdMain());}
        else{bAnswer4.setVisibility(View.INVISIBLE);}
        linLayout.addView(item);
    }

    @AddTrace(name = firebaseReportPrefix+"getDbQuestAnswer")
    public void getDbQuestAnswer(int QuestID){ //Getting question and 3 or 4 answers from DB.
        question = db.getQuestion(Integer.toString(QuestID));
        List<AnswersDataModel> answers = db.getAnswersList(Integer.toString(question.getAnswersGroupId()));
        answer1 = answers.get(0);
        answer2 = answers.get(1);
        answer3 = answers.get(2);
        if(answers.size() == 4){
            answer4 = answers.get(3);
        }
        else {answer4.setAnswerValue(null);}
    }

    @AddTrace(name = firebaseReportPrefix+"nextQuestion")
    public void nextQuestion(String answer, int id){ //Remove old and add new question and 3 or 4 answer depend form addQuestion() method.
        String isTrue = "-";
        Boolean isTruee = false;
        if(id == question.getAnswerId()){isTrue = "+"; isTruee = true; countOfTrueAnswers++;}
        resultforSending += isTrue + " |Вопрос: " + question.getQuestionValue() + " | Ответ: " + answer +  " |"  + "\n\n";
        answerQuestionDataList.add(new AnswerQuestionDataModel(countOfQuestions,answer,question.getQuestionValue(),isTruee));
        linLayout.removeAllViews();
        if(countOfQuestions == 30){
            timerThread.setRunStatus(false);
            isClicked=true; asyncSender =  new AsyncSender(this).execute(); countOfQuestions++; }
        else{
            countOfQuestions ++;
            // Log.d(getString(R.string.log_random_tag),"CountOfQuestions: " + Integer.toString(countOfQuestions));
            // Log.d(getString(R.string.log_random_tag),"arrOfQues:" + Integer.toString(arrOfQues[countOfQuestions]));
            addQuestion(arrOfQues[countOfQuestions]);
        }
    }

    @AddTrace(name = firebaseReportPrefix+"addResult")
    public void addResult(List<AnswerQuestionDataModel> answerQuestionDataModels){
        linLayout = (LinearLayout) findViewById(R.id.quesLay);
        layInflat = getLayoutInflater();
        View item = layInflat.inflate(R.layout.post_item, linLayout,false);
        TextView itemGrade = (TextView) item.findViewById(R.id.textGrade); itemGrade.setText(getString(R.string.item_grade) + Integer.toString(grade));
        RecyclerView recyclerViewer = (RecyclerView) item.findViewById(R.id.scrollFinalList);
        LinearLayoutManager llm = new LinearLayoutManager(item.getContext());
        recyclerViewer.setLayoutManager(llm);
        recyclerViewer.setHasFixedSize(true);
        RVAdapter adapter = new RVAdapter(answerQuestionDataModels);
        recyclerViewer.setAdapter(adapter);
        linLayout.addView(item);
    }

    @AddTrace(name = firebaseReportPrefix+"getRandQuestion")
    public int[] getRandQuestion() { //Getting random number in range of Questions table
        final int N = db.getSize(DbHelper.TABLE_QUESTIONS); // Получаем количество записей в таблице
        int[] nums = new int[N];
        // initialize each value at index i to the value i
        for (int i = 0; i < nums.length; ++i)
        {
            nums[i] = i;
           // Log.d(getString(R.string.log_random_tag),"nums[i]:" + Integer.toString(nums[i]));
        }

        Random randomGenerator = new Random();
        int randomIndex; // the randomly selected index each time through the loop
        int randomValue; // the value at nums[randomIndex] each time through the loop

        // randomize order of values
        for(int i = 0; i < nums.length; ++i)
        {
            randomIndex = randomGenerator.nextInt(nums.length);
            randomValue = nums[randomIndex];
            nums[randomIndex] = nums[i];
            nums[i] = randomValue;
           // Log.d(getString(R.string.log_random_tag),"RandomizeOrder:" + Integer.toString(nums[i]));
        }
        return nums;
    }

    @Override
    @AddTrace(name = firebaseReportPrefix+"onBackPressed")
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            exitDialog();
        }
    }

    @Override
    @AddTrace(name = firebaseReportPrefix+"onCreateOptionsMenu")
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.testsings, menu);
        return true;
    }

    @Override
    @AddTrace(name = firebaseReportPrefix+"onOptionsItemSelected")
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    @AddTrace(name = firebaseReportPrefix+"onNavigationItemSelected")
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_start) {
            if(isStart == 0) {
                addQuestion(arrOfQues[countOfQuestions]);
                timerThread.getThread().start();
                isStart = 1;
            }
            else{Snackbar.make(getWindow().getDecorView().getRootView(),getString(R.string.snack_fail_testrun), Snackbar.LENGTH_LONG).show();}
         }
         else if(id == R.id.nav_site){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.vtc_url)));
            startActivity(intent);
        }
         else if (id == R.id.nav_exit) {
             exitDialog();
        }
         else if (id == R.id.nav_colledge){
            DialogView about_coll = new DialogView();
            about_coll.showDialog(this,getText(R.string.about_college).toString());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @AddTrace(name = firebaseReportPrefix+"exitDialog")
    public void exitDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){ builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);}
        else {builder = new AlertDialog.Builder(this);}
        builder.setTitle(getString(R.string.alert_exit));
        builder.setCancelable(false);
        builder.setMessage(R.string.exit_question);
        builder.setPositiveButton(getString(R.string.alert_yes),new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogInterface, int id){android.os.Process.killProcess(android.os.Process.myPid());}
        });
        builder.setNegativeButton(getString(R.string.alert_cancel),new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialint, int id){dialint.cancel();}
        });
        AlertDialog alertDial = builder.create();
        alertDial.show();

    }

    @Override
    @AddTrace(name = firebaseReportPrefix+"onPause")
    public void onPause(){
        super.onPause();
     //   timerThread.setActivityState(false);
        if (asyncSender != null) {
            waitingDialog.dismiss();
            asyncSender.cancel(true);
        }
    }

    @Override
    @AddTrace(name = firebaseReportPrefix+"onResume")
    public void onResume(){
        super.onResume();
        timerThread.setActivityState(true);
        if(isClicked) {
            asyncSender = new AsyncSender(this).execute(); isClicked =false;
        }
    }

    @Override
    @AddTrace(name = firebaseReportPrefix+"onDestroy")
    public void onDestroy(){
        super.onDestroy();
      //  timerThread.setActivityState(false);
    }


}
