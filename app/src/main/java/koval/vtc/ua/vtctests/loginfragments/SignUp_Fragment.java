package koval.vtc.ua.vtctests.loginfragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;

import koval.vtc.ua.vtctests.CustomToast;
import koval.vtc.ua.vtctests.R;
import koval.vtc.ua.vtctests.mainclasses.TestsingsActivity;

public class SignUp_Fragment extends Fragment implements OnClickListener {
	private static View view;
	private static TextView login;
	private static Button signUpButton;
	private static CheckBox terms_conditions;
	private static EditText firstName, lastName, thirdName, group;

	public SignUp_Fragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.signup_layout, container, false);
		initViews();
		setListeners();
		return view;
	}

	// Initialize all views
	private void initViews() {
		firstName = (EditText) view.findViewById(R.id.firstName);
		lastName = (EditText) view.findViewById(R.id.lastName);
		thirdName = (EditText) view.findViewById(R.id.thirdName);
		group = (EditText) view.findViewById(R.id.groupName);
		signUpButton = (Button) view.findViewById(R.id.signUpBtn);

		// Setting text selector over textviews
		@SuppressLint("ResourceType") XmlResourceParser xrp = getResources().getXml(R.drawable.text_selector);
		try {
			ColorStateList csl = ColorStateList.createFromXml(getResources(),
					xrp);

			login.setTextColor(csl);
			terms_conditions.setTextColor(csl);
		} catch (Exception e) {
			FirebaseCrash.report(e);
		}
	}

	// Set Listeners
	private void setListeners() {
		signUpButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.signUpBtn:

			// Call checkValidation method
			checkValidation();
			break;
		}

	}

	// Check Validation Method
	private void checkValidation() {

		// Get all edittext texts
		String getFirstName = firstName.getText().toString();
		String getLastName = lastName.getText().toString();
		String getThirdName = thirdName.getText().toString();
		String getGroupName = group.getText().toString();

		// Check if all strings are null or not
		if(getFirstName.toString().length() >= 1 && getLastName.toString().length() >= 1){
			if(getGroupName.toString().length() == 4 || getGroupName.toString().length() == 3){
				Intent intent = new Intent(getContext(), TestsingsActivity.class);
				intent.putExtra(getString(R.string.intent_username), getFirstName);
				intent.putExtra(getString(R.string.intent_userlastname), getLastName);
				intent.putExtra(getString(R.string.intent_usergroup),getGroupName);
				startActivity(intent); }
		else{
			new CustomToast().Show_Toast(getActivity(), view,
					getString(R.string.toast_wrong_group_format));}}

		// Make sure user should check Terms and Conditions checkbox
		else
			new CustomToast().Show_Toast(getActivity(), view,
					getString(R.string.toast_fields_exsist));


	}
}
