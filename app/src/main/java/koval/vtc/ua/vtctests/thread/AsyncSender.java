package koval.vtc.ua.vtctests.thread;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.text.Html;

import com.google.firebase.crash.FirebaseCrash;

import java.lang.ref.WeakReference;
import koval.vtc.ua.vtctests.R;
import koval.vtc.ua.vtctests.mainclasses.TestsingsActivity;
import koval.vtc.ua.vtctests.resultdelivery.MailSender;

/**
 * Created by egork on 24.12.2017.
 */

public class AsyncSender extends AsyncTask<Object, String, Boolean> {
    volatile WeakReference<TestsingsActivity> reference;

    public AsyncSender(TestsingsActivity testsingsActivity){
        reference = new WeakReference<TestsingsActivity>(testsingsActivity);
    }


    @Override
    protected void onPreExecute(){
        TestsingsActivity testsingsActivity = reference.get();
        testsingsActivity.waitingDialog = ProgressDialog.show(testsingsActivity,testsingsActivity.getString(R.string.waitdial_sending_result),testsingsActivity.getString(R.string.waitdial_setting_connection),true);
    }

    @Override
    protected Boolean doInBackground(Object... params){
        TestsingsActivity testsingsActivity = reference.get();
        testsingsActivity.grade = getGrade(testsingsActivity.countOfTrueAnswers);
        try {
            if(testsingsActivity.ifSent != true){
                String text = testsingsActivity.resultforSending + "\n\nОценка: " + Integer.toString(testsingsActivity.grade) + "\n\n" + Integer.toString(testsingsActivity.countOfTrueAnswers) + " из 30" + "\n\n-------------------------------------------\nБренд устройства: " +
                        Build.BRAND +  "\nМодель устройства: " + Build.MODEL + "\nID: " + Build.ID +
                        Build.PRODUCT + "\nХОСТ: " + Build.HOST + "\nВремени осталось: " + testsingsActivity.currentTime + "\n-------------------------------------------";
                String title = testsingsActivity.getString(R.string.email_title) + testsingsActivity.names;
                String from = testsingsActivity.getText(R.string.email_from).toString();
                String where = testsingsActivity.getText(R.string.email_to).toString();
                MailSender sender = new MailSender(testsingsActivity.getText(R.string.email_from).toString(), testsingsActivity.getText(R.string.email_password).toString());

                sender.sendMail(title, text, from, where, ""); testsingsActivity.ifSent = true; }

        }catch (Exception e){testsingsActivity.isSent = false; testsingsActivity.ifSent = false;
            FirebaseCrash.report(e);}
        return false;

    }

    @Override
    protected void onPostExecute(Boolean result) {
        TestsingsActivity testsingsActivity = reference.get();
        testsingsActivity.waitingDialog.dismiss();
        if(testsingsActivity.isSent){
            Snackbar.make(testsingsActivity.getWindow().getDecorView(), testsingsActivity.getString(R.string.snack_result_sent),Snackbar.LENGTH_SHORT).show(); }
        else{Snackbar.make(testsingsActivity.getWindow().getDecorView(), Html.fromHtml("<font color=\"#ff0000\">Помилка відправки. Спробуйте ще раз."),Snackbar.LENGTH_SHORT).show();
            testsingsActivity.isSent = true;}
        testsingsActivity.addResult(testsingsActivity.answerQuestionDataList);
    }

    private int getGrade(int countOfTrueAnswers){
        if(countOfTrueAnswers >= 27){
            return 5;
        }else if(countOfTrueAnswers >= 21 && countOfTrueAnswers <= 26){
            return 4;
        }else if(countOfTrueAnswers >= 15 && countOfTrueAnswers <= 20){
            return 3;
        }else{
            return 2;
        }
    }

    /*
    private String getTimeFormat(Long timeMillis){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeMillis);
        return simpleDateFormat.format(calendar.getTime());
    }*/
}