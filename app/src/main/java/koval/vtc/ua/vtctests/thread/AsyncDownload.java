package koval.vtc.ua.vtctests.thread;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.firebase.crash.FirebaseCrash;

import java.lang.ref.WeakReference;
import java.util.List;

import koval.vtc.ua.vtctests.CustomToast;
import koval.vtc.ua.vtctests.R;
import koval.vtc.ua.vtctests.datamodels.KeyDataModel;
import koval.vtc.ua.vtctests.dbsupport.DbActions;
import koval.vtc.ua.vtctests.dbsupport.FileUtil;
import koval.vtc.ua.vtctests.loginfragments.SignUp_Fragment;
import koval.vtc.ua.vtctests.mainclasses.LoginAct;

/**
 * Создаем процесс загрузки БД из сети и авторизации.
 */
public class AsyncDownload extends AsyncTask<String, Void, KeyDataModel> {
    private DbActions db;
    String passw;
    volatile WeakReference<LoginAct> mainActivityWeakReference;

   public AsyncDownload(LoginAct loginAct) {
        mainActivityWeakReference = new WeakReference<LoginAct>(loginAct);
        passw = loginAct.password.getText().toString();
    }


    @Override
    protected void onPreExecute() {
        //show progress dialog
        LoginAct loginAct = mainActivityWeakReference.get();
        if (loginAct != null)
            FileUtil.eraise(loginAct.getString(R.string.delete_db_path_database), loginAct.getString(R.string.delete_db_path_files), loginAct.getString(R.string.delete_db_journal_path));
            loginAct.waitDialogAuth = ProgressDialog.show(loginAct.getContext(), loginAct.getString(R.string.waitdial_auth), loginAct.getString(R.string.waitdial_connecting), true);
    }

    @Override
    protected KeyDataModel doInBackground(String... pathToFile) {
        KeyDataModel kdm = new KeyDataModel(666, " ", 1);
        if (FileUtil.downloadFile(pathToFile[0], pathToFile[1])) { // Cкачиваем БД, если true - вытаскиваем из базы ключи и проверяем

            LoginAct act = mainActivityWeakReference.get();

            if (act == null)
                return null;
            try {
                db = new DbActions(act.getContext());
            }catch (Exception e){
                FirebaseCrash.report(e);
            }

            kdm.setId(777);
            List<KeyDataModel> keyDATAMODEL = db.getKeysList();
            for (KeyDataModel KeDM : keyDATAMODEL) {
                if (KeDM.getValueKey().equals(passw)) {
                    kdm = KeDM;
                    break;
                }
            }
            db.close();
        }
        return kdm;
    }

    @Override
    protected void onPostExecute(KeyDataModel kdm) {
        if (kdm == null)
            return;

        LoginAct act = mainActivityWeakReference.get();

        if (act == null)
            return;


        act.waitDialogAuth.setMessage(act.getString(R.string.waitdial_key_check));
        super.onPostExecute(kdm);
        if (kdm.getId() == 666) { // Если не удалось скачать БД
             act.waitDialogAuth.dismiss();
            new CustomToast().Show_Toast(act.getContext(), act.view, act.getString(R.string.toast_sync_fail));
        } else if (kdm.getId() == 777) { // Если неверный пароль
            act.waitDialogAuth.dismiss();
            new CustomToast().Show_Toast(act.getContext(), act.view, act.getString(R.string.toast_wrong_pass));
        } else { // Если пароль верный
            act.waitDialogAuth.dismiss();
            act.fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                    .replace(R.id.frameContainer, new SignUp_Fragment(), act.getString(R.string.signup_fragment)).commit();
        }
    }
}
