package koval.vtc.ua.vtctests.dbsupport;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.google.firebase.crash.FirebaseCrash;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import koval.vtc.ua.vtctests.datamodels.AnswersDataModel;
import koval.vtc.ua.vtctests.datamodels.KeyDataModel;
import koval.vtc.ua.vtctests.datamodels.QuestionsDataModel;

public class DbActions {
    DbHelper dbHelper;
    Context context;
    Cursor cursor;
    SQLiteDatabase db;
    List<AnswersDataModel> mAnswersList;
    List<QuestionsDataModel> mQuestionsList;
    List<KeyDataModel> mKeyList;
    public static final String DB_LOG = "DB_MESSAGES";

    public DbActions(Context context){
        this.context = context;
        dbHelper = new DbHelper(context);
        try{
            dbHelper.createDataBase();
        }catch (Exception ioe){
            FirebaseCrash.report(ioe);
        }
        try {
            dbHelper.openDataBase();
        }catch (SQLException slqe){
            FirebaseCrash.report(slqe);
        }
    }

    public List<QuestionsDataModel> getQuestionsList(){        //Getting all Questions which contains in DB(table "tests_db").
        db = dbHelper.getReadableDatabase();
        cursor = db.query(DbHelper.TABLE_QUESTIONS,null,null,null,null,null,null);
        mQuestionsList = new ArrayList<QuestionsDataModel>();


        if(cursor.moveToFirst()){
            int idColInd = cursor.getColumnIndex(DbHelper.COLUMN_MAIN_ID);
            int questionValueColInt = cursor.getColumnIndex(DbHelper.COLUMN_QUESTIONS_QUESTION_VALUE);
            int questionAnswerID = cursor.getColumnIndex(DbHelper.COLUMN_QUESTIONS_ANSWER_ID);
            int questionAnswerGroupID = cursor.getColumnIndex(DbHelper.COLUMN_QUESTIONS_ANSWERS_GROUP_ID);
            do{
                QuestionsDataModel QueDM = new QuestionsDataModel(cursor.getInt(idColInd),cursor.getString(questionValueColInt),cursor.getInt(questionAnswerID),
                        cursor.getInt(questionAnswerGroupID)); mQuestionsList.add(QueDM);
            }while (cursor.moveToNext());
        } else{}
        cursor.close();
        return mQuestionsList;
    }

    public List<AnswersDataModel> getAnswersList(String groupID){         //Getting all Answers which contains in DB(table "answers_db").
        db = dbHelper.getReadableDatabase();
        if(groupID == null){
        cursor = db.query(DbHelper.TABLE_ANSWERS,null,null,null,null,null,null);}
        else {
            String[] groupArgs = new String[]{groupID};
            cursor = db.query(DbHelper.TABLE_ANSWERS,null,"answerGroupIDmain = ?", groupArgs,null,null,null);
        }
        mAnswersList = new ArrayList<AnswersDataModel>();

        if(cursor.moveToFirst()){
            int idColInd = cursor.getColumnIndex(DbHelper.COLUMN_MAIN_ID);
            int answerValueColInd = cursor.getColumnIndex(DbHelper.COLUMN_ANSWERS_ANSWER_VALUE);
            int answerIDColInd = cursor.getColumnIndex(DbHelper.COLUMN_ANSWERS_ANSWER_ID_MAIN);
            int answerGroupIDColInd = cursor.getColumnIndex(DbHelper.COLUMN_ANSWERS_ANSWER_GROUP_ID_MAIN);
            do{
                AnswersDataModel AnswDM = new AnswersDataModel(cursor.getInt(idColInd),cursor.getString(answerValueColInd),cursor.getInt(answerIDColInd),
                        cursor.getInt(answerGroupIDColInd)); mAnswersList.add(AnswDM);
            }while(cursor.moveToNext());
        } else{}
        cursor.close();
        return mAnswersList;
    }

    public List<KeyDataModel> getKeysList(){             //Getting all Keys which contains in DB(table "keys_db").
        db = dbHelper.getReadableDatabase();
        cursor = db.query(DbHelper.TABLE_KEYS,null,null,null,null,null,null);
        mKeyList = new ArrayList<KeyDataModel>();

        if(cursor.moveToFirst()){
            int idColInd = cursor.getColumnIndex(DbHelper.COLUMN_MAIN_ID);
            int keyValueColInd = cursor.getColumnIndex(DbHelper.COLUMN_KEYS_KEY_VALUE);
            int keyStatusColInd = cursor.getColumnIndex(DbHelper.COLUMN_KEYS_KEY_STATUS);
            do{
                KeyDataModel KeyDM = new KeyDataModel(cursor.getInt(idColInd),cursor.getString(keyValueColInd),cursor.getInt(keyStatusColInd));
                mKeyList.add(KeyDM);
            }while(cursor.moveToNext());
        } else{}
        cursor.close();
        return mKeyList;
    }

    public int getSize(String table){ // Getting size of table.
        db = dbHelper.getReadableDatabase();
        cursor = db.query(table,null,null,null,null,null,null);
        int i= cursor.getCount();
        cursor.close();
        return i;
    }

    public QuestionsDataModel getQuestion(String id){    //Getting Question from DB via ID.
        db = dbHelper.getReadableDatabase();
        String[] idArgs = new String[]{id};
        cursor = db.query(DbHelper.TABLE_QUESTIONS,null,"_id = ?",idArgs,null,null,null);
        QuestionsDataModel quemd = new QuestionsDataModel(null,null,null,null);
        if(cursor.moveToFirst()){
            quemd = new QuestionsDataModel(cursor.getInt(cursor.getColumnIndex(DbHelper.COLUMN_MAIN_ID)),cursor.getString(cursor.getColumnIndex(DbHelper.COLUMN_QUESTIONS_QUESTION_VALUE)),
                    cursor.getInt(cursor.getColumnIndex(DbHelper.COLUMN_QUESTIONS_ANSWER_ID)),cursor.getInt(cursor.getColumnIndex(DbHelper.COLUMN_QUESTIONS_ANSWERS_GROUP_ID)));
        }else{}
        return quemd;
    }


    public void close(){
        dbHelper.close();
        db.close();
    }
}
